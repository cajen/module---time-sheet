<?php

namespace Modules\TimeSheet\Entities;

use Illuminate\Database\Eloquent\Model;

class TimeSheetEntry extends Model
{
    protected $table = "timesheet_entries";

    protected $fillable = ["user_id", "timesheet_id", "description"];

    protected $dates = ["specified_time"];


    public function lcase()
    {
        return $this->timesheet->lcase();
    }
}
