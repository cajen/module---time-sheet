<?php

namespace Modules\TimeSheet\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TimeSheet\Entities\TimeSheetEntry;

class TimeSheetController extends Controller
{
    use ValidatesRequests;
    use HasTimestamps;

    public function __construct()
    {
        $this->middleware('CheckLCase');
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $case = $request->input('case');
        $user = get_auth_user();

        $time_sheet = TimeSheetEntry::where('case_id', $case->id)->where('user_id', $user->id)->get();

        return view('timesheet::index')->with(compact('case', 'time_sheet', 'user'));
    }

    public function saveTimeSheetEntry(Request $request)
    {
        $this->validate($request, [
            "description" => "required",
            "minutes" => "required",
            "specified_time" => "required"
        ]);

        $user = get_auth_user();
        $date = $request->input("specified_time");
        $date = Carbon::createFromFormat("Y-m-d i:H:s", $date);

        $entry = new TimeSheetEntry();
        $entry->user_id = $user->id;
        $entry->case_id = $request->input("case")->id;
        $entry->description = $request->input("description");
        $entry->minutes = $request->input("minutes");
        $entry->specified_time = $date;

        if($entry->save())
        {
            return response()->success();
        }
    }

    public function deleteTimeSheetEntry(Request $request)
    {
        $entry_id = $request->input('entry_id');
        $case = $request->input('case');
        $user = get_auth_user();

        TimeSheetEntry::where('id', $entry_id)->where('case_id', $case->id)->where('user_id', $user->id)->delete();

        return response()->success();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewTotals(Request $request)
    {
        $case = $request->input('case');
        $entries = TimeSheetEntry::where('case_id', $case->id)->orderBy('user_id')->get();
        $users = $case->users;

        $map = [];

        foreach($entries as $entry)
        {
            if(!array_key_exists($entry->user_id, $map)) {
                $map[$entry->user_id] = [];
            }

            array_push($map[$entry->user_id], $entry);
        }


        return view('timesheet::totals')->with(compact('case', 'users', 'map'));
    }
}
