<?php

namespace Modules\TaskBoard\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\TaskBoard\Entities\LCase;

class CheckLCase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();
        $case_id = $request->route('case_id');

        $case = $user->cases()->where('cases.id', $case_id)->firstOrFail();
        $tCase = LCase::where('id', $case->id)
            ->with([
                'taskboards',
                'taskboards.lanes',
                'taskboards.lanes.tasks'
            ])
            ->firstOrFail();

        $request->merge(['case' => $tCase]);
        return $next($request);
    }
}
