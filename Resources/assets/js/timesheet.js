import {C} from "../../../../../resources/assets/js/modules/ajax-api";

export let initTimeSheet = function()
{
    let timesheet_save_url =  $('meta[name=timesheet_save_url]').attr("content");
    let timesheet_delete_url =  $('meta[name=timesheet_delete_url]').attr("content");
    let timesheet_update_url =  $('meta[name=timesheet_update_url]').attr("content");

    function init_datetime() {
        $('#datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            locale: 'nl-NL',
            sideBySide: true,
            defaultDate: new Date()
        });
    }

    init_datetime();

    $('.input-group-addon').click(function() {
        $('.bootstrap-datetimepicker-widget').find('span').each(function(k, v) {
            let $element = $(v);
            let is_fa_element = $element.hasClass('fas') || $element.hasClass('far');

            if(is_fa_element)
            {
                let current_classes = $element.attr('class');
                let new_element = "<i class='" + current_classes + "'></i>";
                $element.replaceWith(new_element);
            }
        });
    });

    $('#add-log').click(function() {
        let post_array = {};

        $('.entry-field').each(function() {
            let id = $(this).attr("id");
            let var_name = id.replace("create-entry-", "");
            let value = $(this).val();

            post_array[var_name] = value;
        });

        C.post(timesheet_save_url, post_array, function() {
            location.reload();
        })
    });

    $('#log-tbody').on('click', '.delete-x', function() {
        let succeed = confirm("Are you sure you want to delete this entry?");

        if(succeed)
        {
            let entry_id = $(this).parents('.log-entry-row').attr('data-target');
            C.delete(timesheet_delete_url, {entry_id: entry_id}, function() {
                location.reload();s
            });
        }
    })
};