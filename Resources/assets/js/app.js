import {initTimeSheet} from "./timesheet";

App.actions['TimeSheetController'] = {
    "index": {
        initTimeSheet
    }
};