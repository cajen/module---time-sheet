@extends('layouts.app')

@section('head_scripts')
    <script src="{{asset('js/timesheet.js')}}"></script>
@endsection

@section('head_stylesheets')
    <link href="{{ mix('css/timesheet.css') }}" rel="stylesheet">
@endsection

@section('breadcrumbs')
    {{\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('case.timesheet', $case)}}
@endsection

@section('content')
    <meta name="timesheet_save_url" content="{!! route('timesheet.save', [$case->id]) !!}">
    <meta name="timesheet_delete_url" content="{!! route('timesheet.delete', [$case->id]) !!}">
    <meta name="timesheet_update_url" content="{!! route('timesheet.update', [$case->id]) !!}">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div>
                    <h2 style="display:inline">Time Sheet</h2>
                    @role('admin')
                        <a href="{{route('timesheet.totals', [$case->id])}}" class="btn btn-success btn-sm float-right">Totals</a>
                    @endrole
                </div>
                <div class="row">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Task</th>
                            <th>Minutes Spent</th>
                            <th>Time Specification</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="log-tbody">
                        @include('timesheet::entry_table')
                        <!-- Creating an entry -->
                        <tr>
                            <td><input class="empty-after-post entry-field form-control" type="text" id="create-entry-description"></td>
                            <td><input class="empty-after-post entry-field form-control" type="text" id="create-entry-minutes"></td>
                            <td>
                                <div class='input-group date' id='datetimepicker'>
                                    <input class="entry-field form-control" id="create-entry-specified_time" type='text' />
                                    <span class="input-group-addon">
                                                <span class="far fa-calendar-alt"></span>
                                            </span>
                                </div>
                            </td>
                            <td id="add-row-cell"><i id="add-log" class="green-text fa-2x fas fa-plus-circle"></i></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
