@extends('layouts.app')

@section('head_scripts')
    <script src="{{asset('js/timesheet.js')}}"></script>
@endsection

@section('head_stylesheets')
    <link href="{{ mix('css/timesheet.css') }}" rel="stylesheet">
@endsection

@section('breadcrumbs')
    {{\DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('case.timesheet', $case)}}
@endsection

@section('content')
    <meta name="timesheet_save_url" content="{!! route('timesheet.save', [$case->id]) !!}">
    <meta name="timesheet_delete_url" content="{!! route('timesheet.delete', [$case->id]) !!}">
    <meta name="timesheet_update_url" content="{!! route('timesheet.update', [$case->id]) !!}">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div>
                    <h2 style="display:inline">Time Sheet</h2>
                </div>
                @foreach($map as $user_id => $time_sheet)
                    <h3>{{$users->where('id', $user_id)->first()->name}}</h3>
                    <div class="row">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Task</th>
                                <th>Minutes Spent</th>
                                <th>Time Specification</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="log-tbody">
                                @include('timesheet::entry_table')
                            </tbody>
                        </table>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
