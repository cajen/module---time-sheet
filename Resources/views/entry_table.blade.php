@foreach($time_sheet as $entry)
    <tr data-target="{{$entry->id}}" class="log-entry-row">
        <td><span class="edit-data-span" data-type="text" data-name="task">{{$entry->description}}</span></td>
        <td><span class="edit-data-span" data-type="text" data-name="minutes">{{$entry->minutes}}</span></td>
        <td><span class="edit-data-span" data-type="datetime" data-name="specified_time">{{$entry->specified_time}}</span></td>
        <td>
            <i class="delete-x fas fa-2x fa-times"></i>
            <!--<i class="edit-x far fa-2x fa-edit"></i>-->
        </td>
    </tr>
@endforeach