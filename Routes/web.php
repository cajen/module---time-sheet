<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web', 'auth']], function () {
    Route::prefix('/case/{case_id}/timesheet')->group(function () {
        Route::get('/', 'TimeSheetController@index')->name('time_sheet_main');

        Route::put('/', ['uses' => 'TimeSheetController@updateTimeSheetEntry'])->name('timesheet.update');
        Route::post('/', ['uses' => 'TimeSheetController@saveTimeSheetEntry'])->name('timesheet.save');
        Route::delete('/', ['uses' => 'TimeSheetController@deleteTimeSheetEntry'])->name('timesheet.delete');

        Route::get('/totals', ['uses' => 'TimeSheetController@viewTotals', 'middleware' => ['role:admin']])->name('timesheet.totals');
    });
});
