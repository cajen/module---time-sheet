<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Breadcrumbs::register('case.timesheet', function($breadcrumbs, $case) {
    $breadcrumbs->parent('case.show', $case);
    $breadcrumbs->push("Time Sheet", route('time_sheet_main', [$case->id]));
});
